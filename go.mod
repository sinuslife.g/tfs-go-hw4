module gitlab.com/sinuslife.g/tfs-go-hw4

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.1.0+incompatible
	github.com/golang/protobuf v1.4.1
	github.com/gorilla/websocket v1.4.2
	github.com/jinzhu/gorm v1.9.12
	github.com/pkg/errors v0.9.1
	go.uber.org/zap v1.14.1
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.22.0
)
