package main

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/repository"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/robot"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/scheduler"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/service"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/session"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/user"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/ws"
	"gitlab.com/sinuslife.g/tfs-go-hw4/pkg/log"
)

const (
	serverPort = ":8080"

	WSBufferSize = 1024

	dbDialect = "postgres"
	dsn       = "postgres://user:qwerty@db:5432/fintech" +
		"?sslmode=disable"

	priceServiceURL = "localhost:5000"
)

func main() {
	logger := log.New()

	db, err := gorm.Open(dbDialect, dsn)
	if err != nil {
		logger.Fatalf("%v", err)
	}

	defer db.Close()
	applyMigrate(db)

	//repository
	sessionRepository := repository.NewSessionRepository(db)
	robotRepository := repository.NewRobotRepository(db)

	//service
	tokenService := service.NewTokenService("secret", sessionRepository)
	userService := service.NewUserService(repository.NewUserRepository(db), service.SHA256PassEncryptor{}, sessionRepository, *tokenService)
	robotService := service.NewRobotService(robotRepository)

	//ws
	activeRobotConnections := ws.NewActiveRobotConnections()
	activeRobotDetailsConnections := ws.NewActiveRobotDetailsConnections()
	upgrader := websocket.Upgrader{
		ReadBufferSize:  WSBufferSize,
		WriteBufferSize: WSBufferSize,
	}

	handler := NewHandler(logger, &upgrader, *userService, *tokenService, *robotService, activeRobotConnections, activeRobotDetailsConnections)

	robotScheduler := scheduler.NewRobotScheduler(logger, robotRepository, activeRobotConnections, activeRobotDetailsConnections)
	go robotScheduler.Run(priceServiceURL)

	err = http.ListenAndServe(serverPort, routes(handler))
	if err != nil {
		logger.Error(err.Error())
	}
}

func routes(h *Handler) *chi.Mux {
	r := chi.NewRouter()
	r.Mount("/", h.Routes())

	return r
}

func applyMigrate(db *gorm.DB) {
	db.AutoMigrate(&user.User{})
	db.AutoMigrate(&session.Session{})
	db.AutoMigrate(&robot.Robot{})
}
