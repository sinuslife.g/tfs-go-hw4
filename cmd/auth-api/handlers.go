package main

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/exception"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/mapper"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/requests"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/responses"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/service"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/ws"
	"gitlab.com/sinuslife.g/tfs-go-hw4/pkg/log"
)

type key int

const (
	Accept              = "accept"
	ApplicationJSON     = "application/json"
	UserIDToken     key = iota
)

type Handler struct {
	log                           log.Logger
	templates                     map[string]*template.Template
	upgrader                      *websocket.Upgrader
	userService                   service.UserService
	tokenService                  service.TokenService
	robotService                  service.RobotService
	activeRobotConnections        *ws.ActiveRobotConnections
	activeRobotDetailsConnections *ws.ActiveRobotDetailsConnections
}

func NewHandler(
	logger log.Logger,
	upgrader *websocket.Upgrader,
	userService service.UserService,
	tokenService service.TokenService,
	robotService service.RobotService,
	activeRobotConnections *ws.ActiveRobotConnections,
	activeRobotDetailsConnections *ws.ActiveRobotDetailsConnections) *Handler {
	h := Handler{
		log:                           logger,
		templates:                     make(map[string]*template.Template),
		upgrader:                      upgrader,
		userService:                   userService,
		tokenService:                  tokenService,
		robotService:                  robotService,
		activeRobotConnections:        activeRobotConnections,
		activeRobotDetailsConnections: activeRobotDetailsConnections,
	}

	h.templates["robots"] = template.Must(template.ParseFiles("./html/robots.html", "./html/base.html"))
	h.templates["userRobots"] = template.Must(template.ParseFiles("./html/userRobots.html", "./html/base.html"))
	h.templates["robotDetails"] = template.Must(template.ParseFiles("./html/robotDetails.html", "./html/base.html"))

	return &h
}

func (h *Handler) renderTemplate(w http.ResponseWriter, name string, template string, viewModel interface{}) {
	tmpl, ok := h.templates[name]
	if !ok {
		h.writeInternalError(w)
		return
	}

	err := tmpl.ExecuteTemplate(w, template, viewModel)
	if err != nil {
		h.log.Errorf("[handlers.renderTemplate] error occurred while trying execute template: %v", err)
		h.writeInternalError(w)

		return
	}
}

func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/api/v1/", func(r chi.Router) {
		r.Post("/signup", h.singUp)
		r.Post("/signin", h.signIn)

		r.Route("/user", func(r chi.Router) {
			r.Use(h.AuthMiddleware())

			r.Route("/{id}", func(r chi.Router) {
				r.Get("/", h.getUser)
				r.Put("/", h.updateUser)

				r.Get("/robots", h.getAllUserRobots)
			})
		})

		r.Route("/robots", func(r chi.Router) {
			r.Use(h.AuthMiddleware())
			r.Get("/", h.getRobots)
		})

		r.Get("/robots/ws", h.getRobotsWS)

		r.Route("/robot", func(r chi.Router) {
			r.Use(h.AuthMiddleware())

			r.Post("/", h.createRobot)
			r.Route("/{id}", func(r chi.Router) {
				r.Get("/", h.getRobot)
				r.Get("/ws", h.getRobotWS)
				r.Delete("/", h.deleteRobot)
				r.Put("/favorite", h.addFavorite)
				r.Put("/activate", h.activateRobot)
				r.Put("/deactivate", h.deactivateRobot)
			})
		})
	})

	return r
}

func (h *Handler) AuthMiddleware() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			authHeader := r.Header.Get("Authorization")
			userID, err := h.tokenService.ValidateToken(authHeader)

			if err != nil {
				h.log.Errorf("some error occurred while trying resolve auth token: %v", err)
				userID = -1
			}

			ctx := context.WithValue(r.Context(), UserIDToken, userID)
			r = r.WithContext(ctx)

			next.ServeHTTP(w, r)
		})
	}
}

func (h *Handler) singUp(w http.ResponseWriter, r *http.Request) {
	var request requests.CreateUserRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		h.log.Errorf("[signUp] some error occurred while trying parse CreateUserRequest: %v", err)
		h.writeInternalError(w)

		return
	}

	registeredUser, err := h.userService.CreateUser(request)
	if err != nil {
		h.log.Errorf("[signUp] some error occurred: %v", err)

		switch errors.Cause(err).(type) {
		case exception.UserAlreadyExistError:
			h.writeError(w, 409, fmt.Sprintf("user %s is already registered", request.Email))
			return
		default:
			h.writeInternalError(w)
			return
		}
	}

	h.writeResponse(w, 201, registeredUser)
}

func (h *Handler) signIn(w http.ResponseWriter, r *http.Request) {
	var request requests.AuthorizeRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		h.log.Errorf("[signIn] some error occurred while trying parse AuthorizeRequest: %v", err)
		h.writeInternalError(w)

		return
	}

	session, err := h.userService.AuthorizeUser(request)
	if err != nil {
		h.log.Errorf("[signIn] some error occurred while trying authorize user: %v", err)

		switch errors.Cause(err).(type) {
		case exception.UserNotFoundError:
			h.writeError(w, 400, "incorrect email or password")
			return
		case exception.InvalidPasswordError:
			h.writeError(w, 400, "incorrect email or password")
			return
		default:
			h.writeInternalError(w)
			return
		}
	}

	h.writeResponse(w, 200, mapper.SessionToSingInResponse(session))
}

func (h *Handler) updateUser(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "id")
	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))

	if userID != userIDFromToken {
		h.writeError(w, 403, "forbidden")
		return
	}

	var request requests.CreateUserRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		h.log.Errorf("[handlers.updateUser] some error occurred  while trying parse CreateUserRequest: %v", err)
		h.writeInternalError(w)

		return
	}

	id, err := strconv.Atoi(userID)
	if err != nil {
		h.log.Errorf("handlers.updateUser")
		h.writeInternalError(w)

		return
	}

	updatedUser, err := h.userService.UpdateUser(id, request)
	if err != nil {
		h.log.Errorf("[handlers.updateUser] error occurred while trying update user: %v", err)
		h.writeInternalError(w)

		return
	}

	h.writeResponse(w, 200, updatedUser)
}

func (h *Handler) getUser(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "id")

	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))
	if userIDFromToken == "-1" {
		h.writeError(w, 403, "forbidden")
		return
	}

	id, _ := strconv.Atoi(userID)

	foundedUser, err := h.userService.GetUser(id)
	if err != nil {
		h.log.Errorf("[handlers.updateUser] error occurred while trying get user by id = %d: %v", id, err)
		h.writeInternalError(w)

		return
	}

	h.writeResponse(w, 200, foundedUser)
}

func (h *Handler) getAllUserRobots(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "id")

	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))
	if userID != userIDFromToken {
		h.writeError(w, 403, "forbidden")
		return
	}

	foundedRobots, err := h.robotService.GetAllRobots(userID, "")
	if err != nil {
		h.log.Errorf("[handlers.getAllUserRobots] error occurred while trying get all robots by userID = %s", userID)
		h.writeInternalError(w)

		return
	}

	if r.Header.Get(Accept) != ApplicationJSON {
		h.renderTemplate(w, "userRobots", "base", foundedRobots)
		return
	}

	h.writeResponse(w, 200, foundedRobots)
}

func (h *Handler) getRobots(w http.ResponseWriter, r *http.Request) {
	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))
	if userIDFromToken == "-1" {
		h.writeError(w, 403, "forbidden")
		return
	}

	if r.Header.Get(Accept) != ApplicationJSON {
		h.renderTemplate(w, "robots", "base", nil)
		return
	}

	userID := r.URL.Query().Get("user")
	ticker := r.URL.Query().Get("ticker")

	foundedRobots, err := h.robotService.GetAllRobots(userID, ticker)
	if err != nil {
		h.log.Errorf("[handlers.getRobots] error occurred while trying get all robots by userID = %s and ticker = %s: %v", userID, ticker, err)
		h.writeInternalError(w)

		return
	}

	h.writeResponse(w, 200, foundedRobots)
}

func (h *Handler) getRobotsWS(w http.ResponseWriter, r *http.Request) {
	conn, err := h.upgrader.Upgrade(w, r, nil)
	if err != nil {
		h.log.Errorf("cannot upgrade connection %v", err)
		return
	}

	userID := r.URL.Query().Get("user")
	ticker := r.URL.Query().Get("ticker")

	foundedRobots, err := h.robotService.GetAllRobots(userID, ticker)
	if err != nil {
		h.log.Errorf("[handlers.getRobotWS] some error occurred while trying get all robots: %v", err)

		conn.Close()

		return
	}

	subID := h.activeRobotConnections.Subscribe(conn, userID, ticker)

	err = conn.WriteJSON(foundedRobots)
	if err != nil {
		h.log.Errorf("[handler.getRobotWS] some error occurred while trying write ws message")

		conn.Close()
		h.activeRobotConnections.Unsubscribe(subID)

		return
	}
}

func (h *Handler) createRobot(w http.ResponseWriter, r *http.Request) {
	var request requests.CreateRobotRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		h.log.Errorf("[createRobot] some error occurred while trying parse CreateRobotRequest: %v", err)
		h.writeInternalError(w)

		return
	}

	userID, ok := r.Context().Value(UserIDToken).(int)
	if !ok {
		h.writeInternalError(w)
		return
	}

	if userID == -1 {
		h.writeError(w, 403, "forbidden")
		return
	}

	createdRobot, _ := h.robotService.CreateRobot(request, userID)

	h.writeResponse(w, 201, createdRobot)

	h.activeRobotConnections.Broadcast(createdRobot)
}

func (h *Handler) deleteRobot(w http.ResponseWriter, r *http.Request) {
	robotID := chi.URLParam(r, "id")

	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))
	if userIDFromToken == "-1" {
		h.writeError(w, 403, "forbidden")
		return
	}

	err := h.robotService.DeleteRobot(robotID, userIDFromToken)
	if err != nil {
		h.log.Errorf("[handlers.deleteRobot] error occurred while trying delete robot: %v", err)
		h.writeInternalError(w)
	}

	h.writeResponse(w, 200, nil)
}

func (h *Handler) addFavorite(w http.ResponseWriter, r *http.Request) {
	robotID := chi.URLParam(r, "id")

	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))
	if userIDFromToken == "-1" {
		h.writeError(w, 403, "forbidden")
		return
	}

	favouriteRobot, err := h.robotService.AddFavorite(robotID, userIDFromToken)
	if err != nil {
		h.log.Errorf("[handlers.addFavorite] error occurred while trying add robot to favorite: %v", err)
		h.writeInternalError(w)

		return
	}

	h.writeResponse(w, 200, favouriteRobot)

	h.activeRobotConnections.Broadcast(favouriteRobot)
}

//nolint:dupl
func (h *Handler) activateRobot(w http.ResponseWriter, r *http.Request) {
	robotID := chi.URLParam(r, "id")

	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))
	if userIDFromToken == "-1" {
		h.writeError(w, 403, "forbidden")
		return
	}

	activatedRobot, err := h.robotService.ActivateRobot(robotID, userIDFromToken)
	if err != nil {
		h.log.Errorf("[handlers.activateRobot] error occurred while trying activate robot: %v", err)
		h.writeInternalError(w)

		return
	}

	h.writeResponse(w, 200, activatedRobot)

	h.activeRobotConnections.Broadcast(activatedRobot)
	h.activeRobotDetailsConnections.Broadcast(activatedRobot)
}

//nolint:dupl
func (h *Handler) deactivateRobot(w http.ResponseWriter, r *http.Request) {
	robotID := chi.URLParam(r, "id")

	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))
	if userIDFromToken == "-1" {
		h.writeError(w, 403, "forbidden")
		return
	}

	deactivatedRobot, err := h.robotService.DeactivateRobot(robotID, userIDFromToken)
	if err != nil {
		h.log.Errorf("[handlers.deactivateRobot] error occurred while trying deactivate robot: %v", err)
		h.writeInternalError(w)

		return
	}

	h.writeResponse(w, 200, deactivatedRobot)

	h.activeRobotConnections.Broadcast(deactivatedRobot)
	h.activeRobotDetailsConnections.Broadcast(deactivatedRobot)
}

func (h *Handler) getRobot(w http.ResponseWriter, r *http.Request) {
	robotID := chi.URLParam(r, "id")

	userIDFromToken := fmt.Sprintf("%v", r.Context().Value(UserIDToken))
	if userIDFromToken == "-1" {
		h.writeError(w, 403, "forbidden")
		return
	}

	robot, err := h.robotService.GetRobot(robotID, userIDFromToken)
	if err != nil {
		h.log.Errorf("[handlers.getRobot] error occurred while trying get robot: %v", err)
		h.writeInternalError(w)

		return
	}

	if r.Header.Get(Accept) == ApplicationJSON {
		h.writeResponse(w, 200, robot)
		return
	}

	h.renderTemplate(w, "robotDetails", "base", robot)
}

func (h *Handler) getRobotWS(w http.ResponseWriter, r *http.Request) {
	conn, err := h.upgrader.Upgrade(w, r, nil)
	if err != nil {
		h.log.Errorf("cannot upgrade connection %v", err)
		return
	}

	id := chi.URLParam(r, "id")

	robotID, err := strconv.Atoi(id)
	if err != nil {
		h.log.Errorf("[handlers.getRobotWS] error occurred while trying convert id from url param: %v", err)
		return
	}

	h.activeRobotDetailsConnections.Subscribe(conn, robotID)
}

func (h *Handler) writeError(w http.ResponseWriter, httpStatus int, message string) {
	h.writeResponse(w, httpStatus, responses.ErrorResult{Error: message})
}

func (h *Handler) writeResponse(w http.ResponseWriter, httpStatus int, response interface{}) {
	marshal, err := json.Marshal(response)
	if err != nil {
		h.log.Errorf("[handlers.writeResponse] some error occurred while trying marshal response: %v", err)
		h.writeInternalError(w)

		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(httpStatus)

	_, err = w.Write(marshal)
	if err != nil {
		h.log.Errorf("[handlers.writeResponse] some error occurred while trying write response %v", err)
	}
}

func (h *Handler) writeInternalError(w http.ResponseWriter) {
	h.writeError(w, 500, "internal error")
}
