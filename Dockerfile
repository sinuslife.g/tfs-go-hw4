FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/tfs-hw4/
COPY . .
RUN GOOS=linux GORCH=amd64 go build -o app ./cmd/auth-api

FROM alpine:3.12
WORKDIR /root/

COPY --from=builder /go/src/tfs-hw4/app .
COPY --from=builder /go/src/tfs-hw4/html/.. .

EXPOSE 8080

CMD ["./app"]
