package format

import (
	"fmt"
	"strings"
	"time"
)

type RFC3339Time struct {
	time.Time
}

func (t *RFC3339Time) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", t.Format(time.RFC3339))
	return []byte(stamp), nil
}

func (t *RFC3339Time) UnmarshalJSON(data []byte) error {
	s := strings.Trim(string(data), "\"")
	if s == "null" {
		t.Time = time.Time{}
		return nil
	}

	parsed, err := time.Parse(time.RFC3339, s)
	t.Time = parsed

	return err
}

type BirthdayTime struct {
	time.Time
}

func (t *BirthdayTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", t.Format("2006-01-02"))
	return []byte(stamp), nil
}

func (t *BirthdayTime) UnmarshalJSON(data []byte) error {
	s := strings.Trim(string(data), "\"")
	if s == "null" {
		t.Time = time.Time{}
		return nil
	}

	parsed, err := time.Parse("2006-01-02", s)
	t.Time = parsed

	return err
}
