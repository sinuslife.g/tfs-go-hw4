package robot

import (
	"time"
)

type Robot struct {
	RobotID       int `gorm:"primary_key"`
	OwnerUserID   int
	ParentRobotID *int
	IsFavorite    bool
	IsActive      bool
	Ticker        string
	BuyPrice      float64
	SellPrice     float64
	PlanStart     *time.Time
	PlanEnd       *time.Time
	PlanYield     float64
	FactYield     float64
	DealsCount    int
	ActivatedAt   *time.Time
	DeactivatedAt *time.Time
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     *time.Time `gorm:"null"`
}

type Repository interface {
	FindByID(id string) (Robot, error)
	FindAll() ([]Robot, error)
	FindAllByUserID(userID string) ([]Robot, error)
	FindAllByTicker(ticker string) ([]Robot, error)
	FindAllByUserIDAndTicker(userID string, ticker string) ([]Robot, error)
	Save(r Robot) (Robot, error)
	Delete(id string) error
	FindAllActiveRobots(now time.Time) ([]Robot, error)
}
