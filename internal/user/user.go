package user

import (
	"time"
)

type User struct {
	ID        int `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time

	Email    string `gorm:"unique;not null"`
	Password string

	FirstName string
	LastName  string
	Birthday  time.Time
}

type Repository interface {
	Save(u User) (User, error)
	Update(u User) (User, error)
	GetByID(id int) (User, error)
	GetByEmail(email string) (User, error)
}

type PasswordEncryptor interface {
	Encode(password string) string
}
