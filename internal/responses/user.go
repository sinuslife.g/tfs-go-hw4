package responses

import (
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/format"
)

type UserResponse struct {
	ID        int                 `json:"id,omitempty"`
	Email     string              `json:"email,omitempty"`
	FirstName string              `json:"first_name,omitempty"`
	LastName  string              `json:"last_name,omitempty"`
	Birthday  format.BirthdayTime `json:"birthday,omitempty"`
}
