package responses

import "gitlab.com/sinuslife.g/tfs-go-hw4/internal/format"

type RobotResponse struct {
	RobotID       int                `json:"robot_id,omitempty"`
	OwnerUserID   int                `json:"owner_user_id,omitempty"`
	ParentRobotID *int               `json:"parent_robot_id,omitempty"`
	Ticker        string             `json:"ticker,omitempty"`
	IsActive      bool               `json:"is_active"`
	DealsCount    int                `json:"deals_count"`
	PlanYield     float64            `json:"plan_yield"`
	FactYield     float64            `json:"fact_yield"`
	IsFavorite    bool               `json:"is_favorite"`
	PlanStart     format.RFC3339Time `json:"plan_start"`
	PlanEnd       format.RFC3339Time `json:"plan_end"`
}
