package exception

import "fmt"

type UserAlreadyExistError struct {
	Email string
	Cause error
}

type UserNotFoundError struct {
	Email string
	Cause error
}

type InvalidPasswordError struct {
}

type InvalidAuthorizationHeaderError struct {
}

func (e UserAlreadyExistError) Error() string {
	return fmt.Sprintf("user with email: %s already exist. Caused by: %s", e.Email, e.Cause)
}

func (e UserNotFoundError) Error() string {
	return fmt.Sprintf("user with email: %s not found. Caused by: %s", e.Email, e.Cause)
}

func (InvalidPasswordError) Error() string {
	return fmt.Sprint("invalid password")
}

func (InvalidAuthorizationHeaderError) Error() string {
	return fmt.Sprintf("invalid authorization header format")
}
