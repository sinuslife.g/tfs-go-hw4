package repository

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/user"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{db: db}
}

func (r UserRepository) Save(u user.User) (user.User, error) {
	db := r.db.Create(&u)
	if db.Error != nil {
		return user.User{}, errors.Wrap(db.Error, "[userRepository.Save] error occurred while trying to save user entity")
	}

	createdUser, ok := db.Value.(*user.User)
	if !ok {
		return user.User{}, errors.New("[userRepository.Save] error occurred while trying to casting to *user.User")
	}

	return *createdUser, nil
}

func (r UserRepository) Update(u user.User) (user.User, error) {
	db := r.db.Save(&u)
	if db.Error != nil {
		return user.User{}, errors.Wrap(db.Error, "[userRepository.Save] error occurred while trying to save user entity")
	}

	updatedUser, ok := db.Value.(*user.User)
	if !ok {
		return user.User{}, errors.New("[userRepository.Save] error occurred while trying to casting to *user.User")
	}

	return *updatedUser, nil
}

func (r UserRepository) GetByID(id int) (user.User, error) {
	var u user.User
	db := r.db.First(&u, "id = ?", id)

	if db.Error != nil {
		return user.User{}, errors.Wrap(db.Error, "[userRepository.GetById] error occurred while trying to get user entity")
	}

	return u, nil
}

func (r UserRepository) GetByEmail(email string) (user.User, error) {
	var u user.User
	db := r.db.First(&u, "email = ?", email)

	if db.Error != nil {
		return user.User{}, errors.Wrap(db.Error, "[userRepository.GetByEmail] error occurred while trying to get user entity")
	}

	return u, nil
}
