package repository

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/robot"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/user"
)

type RobotRepository struct {
	db *gorm.DB
}

func NewRobotRepository(db *gorm.DB) *RobotRepository {
	return &RobotRepository{db: db}
}

func (r RobotRepository) Save(entity robot.Robot) (robot.Robot, error) {
	db := r.db.Save(&entity)
	if db.Error != nil {
		return robot.Robot{}, errors.Wrap(db.Error, "[robotRepository.Save] error occurred while trying to save robot entity")
	}

	savedRobot, ok := db.Value.(*robot.Robot)
	if !ok {
		return robot.Robot{}, errors.New("[robotRepository.Save] error occurred while trying to casting to *robot.Robot")
	}

	return *savedRobot, nil
}

func (r RobotRepository) Delete(id string) error {
	db := r.db.Where("id = ?", id).Delete(&user.User{})
	if db.Error != nil {
		return errors.Wrap(db.Error, "[robotRepository.Delete] error occurred while trying delete entity")
	}

	return nil
}

func (r RobotRepository) FindByID(id string) (robot.Robot, error) {
	var entity robot.Robot
	db := r.db.First(&entity, "robot_id = ?", id)

	if db.Error != nil {
		return robot.Robot{}, errors.Wrap(db.Error, "[robotRepository.FindByID] error occurred while trying get robot entity")
	}

	return entity, nil
}

func (r RobotRepository) FindAll() ([]robot.Robot, error) {
	var robots []robot.Robot
	db := r.db.Order("robot_id").Find(&robots)

	if db.Error != nil {
		return nil, errors.Wrap(db.Error, "[robotRepository.FindAll] error occurred while trying find all robots")
	}

	return robots, nil
}

func (r RobotRepository) FindAllByUserID(userID string) ([]robot.Robot, error) {
	var robots []robot.Robot
	db := r.db.Order("robot_id").Where("owner_user_id = ?", userID).Find(&robots)

	if db.Error != nil {
		return nil, errors.Wrap(db.Error, "[robotRepository.FindAllByUserID] error occurred while trying find all robots for user")
	}

	return robots, nil
}

func (r RobotRepository) FindAllByTicker(ticker string) ([]robot.Robot, error) {
	var robots []robot.Robot
	db := r.db.Order("robot_id").Where("ticker = ?", ticker).Find(&robots)

	if db.Error != nil {
		return nil, errors.Wrap(db.Error, "[robotRepository.FindAllByTicker] error occurred while trying find all robots for ticker")
	}

	return robots, nil
}

func (r RobotRepository) FindAllByUserIDAndTicker(userID string, ticker string) ([]robot.Robot, error) {
	var robots []robot.Robot
	db := r.db.Order("robot_id").Where("owner_user_id = ? AND ticker = ?", userID, ticker).Find(&robots)

	if db.Error != nil {
		return nil, errors.Wrap(db.Error, "[robotRepository.FindAllByUserIDAndTicker] error occurred while trying find all robots for user and ticker")
	}

	return robots, nil
}

func (r RobotRepository) FindAllActiveRobots(now time.Time) ([]robot.Robot, error) {
	var robots []robot.Robot
	db := r.db.Order("robot_id").Where("(is_active = ?) AND (? BETWEEN plan_start AND plan_end)", true, now).Find(&robots)

	if db.Error != nil {
		return nil, errors.Wrap(db.Error, "[robotRepository.FindAllActiveRobots] error occurred while trying find all active robots")
	}

	return robots, nil
}
