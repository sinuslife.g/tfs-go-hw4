package repository

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/session"
)

type SessionRepository struct {
	db *gorm.DB
}

func NewSessionRepository(db *gorm.DB) *SessionRepository {
	return &SessionRepository{db: db}
}

func (r SessionRepository) Save(s session.Session) (session.Session, error) {
	db := r.db.Create(&s)
	if db.Error != nil {
		return session.Session{}, errors.Wrap(db.Error, "[sessionRepository.Save] error occurred while trying to save session entity")
	}

	createdUser, ok := db.Value.(*session.Session)
	if !ok {
		return session.Session{}, errors.New("[sessionRepository.Save] error occurred while trying to casting to *session.Session")
	}

	return *createdUser, nil
}

func (r SessionRepository) FindByUserID(id int) (session.Session, error) {
	var s session.Session
	db := r.db.Where("user_id = ?", id).Find(&s)

	if db.Error != nil {
		return session.Session{}, errors.Wrapf(db.Error, "[sessionRepository.FindByUserID] error occurred while trying find session by user id = %d", id)
	}

	return s, nil
}
