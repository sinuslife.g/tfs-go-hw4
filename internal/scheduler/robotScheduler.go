package scheduler

import (
	"context"
	"fmt"
	"io"
	"math"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/mapper"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/robot"
	fintech "gitlab.com/sinuslife.g/tfs-go-hw4/internal/streamer"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/ws"
	"gitlab.com/sinuslife.g/tfs-go-hw4/pkg/log"
)

const (
	FloatRoundNumber  = 100
	SchedSecondsSleep = 3
)

type RobotScheduler struct {
	log                           log.Logger
	robotRepository               robot.Repository
	activeRobotConnections        *ws.ActiveRobotConnections
	activeRobotDetailsConnections *ws.ActiveRobotDetailsConnections
	activeRobots                  map[int]int
}

func NewRobotScheduler(logger log.Logger, repository robot.Repository, robotConnections *ws.ActiveRobotConnections, detailsConnections *ws.ActiveRobotDetailsConnections) *RobotScheduler {
	return &RobotScheduler{
		log:                           logger,
		robotRepository:               repository,
		activeRobotConnections:        robotConnections,
		activeRobotDetailsConnections: detailsConnections,
		activeRobots:                  make(map[int]int),
	}
}

func (s RobotScheduler) Run(priceServiceURL string) {
	conn, err := grpc.Dial(priceServiceURL, grpc.WithInsecure())
	if err != nil {
		s.log.Error(err.Error())
		return
	}

	defer conn.Close()

	for {
		time.Sleep(time.Second * SchedSecondsSleep)

		robots, err := s.robotRepository.FindAllActiveRobots(time.Now())
		if err != nil {
			s.log.Error(err.Error())
			return
		}

		for _, r := range robots {
			if _, ok := s.activeRobots[r.RobotID]; !ok {
				s.activeRobots[r.RobotID] = r.RobotID
				go s.StartRobot(conn, r.RobotID, r.Ticker)
			}
		}
	}
}

func (s RobotScheduler) StartRobot(conn grpc.ClientConnInterface, robotID int, ticker string) {
	client := fintech.NewTradingServiceClient(conn)
	request := fintech.PriceRequest{Ticker: ticker}

	response, err := client.Price(context.Background(), &request)
	if err != nil {
		s.log.Error(err.Error())
		return
	}

	for {
		r, err := s.robotRepository.FindByID(fmt.Sprint(robotID))
		if err != nil {
			s.log.Errorf(err.Error())
			break
		}

		price, err := response.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			s.log.Error(err.Error())
			break
		}

		if r.PlanEnd.Before(time.Now()) {
			break
		}

		s.trade(r, price)
	}
}

func (s RobotScheduler) trade(r robot.Robot, price *fintech.PriceResponse) {
	if r.BuyPrice == 0 {
		if r.SellPrice == 0 {
			buy(&r, price.BuyPrice)

			err := s.SaveRobot(r)
			if err != nil {
				s.log.Errorf("[robotScheduler.trade] error occurred while trying to save robot: %v", err)
			}

			return
		}

		if price.BuyPrice < r.SellPrice {
			buy(&r, price.BuyPrice)

			err := s.SaveRobot(r)
			if err != nil {
				s.log.Errorf("[robotScheduler.trade] error occurred while trying to save robot: %v", err)
			}

			return
		}

		return
	}

	if r.SellPrice == 0 {
		if price.SellPrice > r.BuyPrice {
			sell(&r, price.SellPrice)

			err := s.SaveRobot(r)
			if err != nil {
				s.log.Errorf("[robotScheduler.trade] error occurred while trying to save robot: %v", err)
			}

			return
		}
	}
}

func buy(r *robot.Robot, buyPrice float64) {
	r.BuyPrice = buyPrice
	r.SellPrice = 0
	r.DealsCount++
}

func sell(r *robot.Robot, sellPrice float64) {
	r.SellPrice = sellPrice

	currentYield := calculateCurrentYield(*r)
	r.FactYield = calculateNewYield(r.FactYield, currentYield, r.DealsCount)

	r.BuyPrice = 0
	r.DealsCount++
}

func (s RobotScheduler) SaveRobot(r robot.Robot) error {
	savedRobot, err := s.robotRepository.Save(r)
	if err != nil {
		return err
	}

	msg := mapper.RobotEntityToResponse(savedRobot)

	s.activeRobotConnections.Broadcast(msg)
	s.activeRobotDetailsConnections.Broadcast(msg)

	return nil
}

func calculateCurrentYield(r robot.Robot) float64 {
	yield := r.SellPrice / r.BuyPrice
	return (math.Floor(yield*FloatRoundNumber) / FloatRoundNumber) - 1
}

func calculateNewYield(currentYield float64, oldYield float64, dealsCount int) float64 {
	yield := oldYield*float64(dealsCount) + currentYield
	dealsCount++
	yield /= float64(dealsCount)

	return math.Floor(yield*FloatRoundNumber) / FloatRoundNumber
}
