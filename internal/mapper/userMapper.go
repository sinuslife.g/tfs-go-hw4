package mapper

import (
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/format"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/responses"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/user"
)

func UserEntityToResponse(entity user.User) responses.UserResponse {
	return responses.UserResponse{
		ID:        entity.ID,
		Email:     entity.Email,
		FirstName: entity.FirstName,
		LastName:  entity.LastName,
		Birthday:  format.BirthdayTime{Time: entity.Birthday},
	}
}
