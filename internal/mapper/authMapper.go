package mapper

import (
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/responses"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/session"
)

func SessionToSingInResponse(session session.Session) responses.SingInResponse {
	return responses.SingInResponse{Bearer: session.SessionID}
}
