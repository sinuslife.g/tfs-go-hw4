package mapper

import (
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/format"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/responses"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/robot"
)

func RobotEntityToResponse(entity robot.Robot) responses.RobotResponse {
	return responses.RobotResponse{
		RobotID:       entity.RobotID,
		OwnerUserID:   entity.OwnerUserID,
		ParentRobotID: entity.ParentRobotID,
		Ticker:        entity.Ticker,
		IsActive:      entity.IsActive,
		DealsCount:    entity.DealsCount,
		PlanYield:     entity.PlanYield,
		FactYield:     entity.FactYield,
		IsFavorite:    entity.IsFavorite,
		PlanStart:     format.RFC3339Time{Time: *entity.PlanStart},
		PlanEnd:       format.RFC3339Time{Time: *entity.PlanEnd},
	}
}

func RobotEntitiesToResponses(entities []robot.Robot) []responses.RobotResponse {
	var result []responses.RobotResponse
	for _, entity := range entities {
		result = append(result, RobotEntityToResponse(entity))
	}

	return result
}
