package requests

import (
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/format"
)

type CreateRobotRequest struct {
	ParentRobotID int                `json:"parent_robot_id"`
	Ticker        string             `json:"ticker"`
	IsFavorite    bool               `json:"is_favorite"`
	PlanStart     format.RFC3339Time `json:"plan_start"`
	PlanEnd       format.RFC3339Time `json:"plan_end"`
	PlanYield     float64            `json:"plan_yield"`
}
