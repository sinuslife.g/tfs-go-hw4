package requests

import (
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/format"
)

type CreateUserRequest struct {
	FirstName string              `json:"first_name"`
	LastName  string              `json:"last_name"`
	Birthday  format.BirthdayTime `json:"birthday"`
	Email     string              `json:"email"`
	Password  string              `json:"password"`
}

type AuthorizeRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
