package ws

import (
	"encoding/json"
	"strconv"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/responses"
)

type ActiveRobotConnections struct {
	Mutex       sync.Mutex
	NextID      int
	Subscribers map[int]RobotSubscriber
}

type RobotSubscriber struct {
	Connection *websocket.Conn
	UserID     *int
	Ticker     *string
}

func NewActiveRobotConnections() *ActiveRobotConnections {
	return &ActiveRobotConnections{
		Mutex:       sync.Mutex{},
		Subscribers: make(map[int]RobotSubscriber),
	}
}

func (c *ActiveRobotConnections) Subscribe(conn *websocket.Conn, userID string, ticker string) int {
	c.Mutex.Lock()
	defer c.Mutex.Unlock()

	var subUserID *int
	if len(userID) == 0 {
		subUserID = nil
	} else {
		atoi, _ := strconv.Atoi(userID)
		subUserID = &atoi
	}

	var subTicker *string

	if len(ticker) == 0 {
		subUserID = nil
	} else {
		subTicker = &ticker
	}

	c.NextID++
	c.Subscribers[c.NextID] = RobotSubscriber{
		Connection: conn,
		UserID:     subUserID,
		Ticker:     subTicker,
	}

	return c.NextID
}

func (c *ActiveRobotConnections) Unsubscribe(connectionIDs ...int) {
	c.Mutex.Lock()
	defer c.Mutex.Unlock()

	for _, id := range connectionIDs {
		c.Subscribers[id].Connection.Close()
		delete(c.Subscribers, id)
	}
}

func (c *ActiveRobotConnections) Broadcast(response responses.RobotResponse) {
	c.Mutex.Lock()
	inactiveConnections := make([]int, 0)

	for id, sub := range c.Subscribers {
		if mustReceiveMessage(sub, response.OwnerUserID, response.Ticker) {
			msg, _ := json.Marshal(response)
			if err := sub.Connection.WriteMessage(websocket.TextMessage, msg); err != nil {
				inactiveConnections = append(inactiveConnections, id)
			}
		}
	}

	c.Mutex.Unlock()
	c.Unsubscribe(inactiveConnections...)
}

func mustReceiveMessage(sub RobotSubscriber, userID int, ticker string) bool {
	var result = true

	if sub.UserID != nil {
		if *sub.UserID == userID {
			result = true
		} else {
			result = false
		}
	}

	if result && sub.Ticker != nil {
		if *sub.Ticker == ticker {
			result = true
		} else {
			result = false
		}
	}

	return result
}
