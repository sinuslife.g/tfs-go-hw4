package ws

import (
	"encoding/json"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/responses"
)

type ActiveRobotDetailsConnections struct {
	Mutex      sync.Mutex
	Subscriber map[int]*DetailsSubscriber
}

type DetailsSubscriber struct {
	NextID      int
	connections map[int]*websocket.Conn
}

func NewActiveRobotDetailsConnections() *ActiveRobotDetailsConnections {
	return &ActiveRobotDetailsConnections{
		Mutex:      sync.Mutex{},
		Subscriber: make(map[int]*DetailsSubscriber),
	}
}

func (c *ActiveRobotDetailsConnections) Subscribe(conn *websocket.Conn, robotID int) {
	c.Mutex.Lock()

	sub, ok := c.Subscriber[robotID]
	if !ok || sub == nil {
		c.Subscriber[robotID] = &DetailsSubscriber{
			connections: make(map[int]*websocket.Conn),
		}
		sub = c.Subscriber[robotID]
	}

	sub.NextID++

	c.Subscriber[robotID].connections[sub.NextID] = conn

	c.Mutex.Unlock()
}

func (c *ActiveRobotDetailsConnections) Unsubscribe(robotID int, connectionsIDs ...int) {
	c.Mutex.Lock()

	for _, id := range connectionsIDs {
		c.Subscriber[robotID].connections[id].Close()
		delete(c.Subscriber[robotID].connections, id)
	}

	c.Mutex.Unlock()
}

func (c *ActiveRobotDetailsConnections) Broadcast(response responses.RobotResponse) {
	if response.OwnerUserID <= 0 {
		return
	}

	c.Mutex.Lock()

	inactiveConnections := make([]int, 0)

	if c.Subscriber[response.RobotID] != nil {
		for id, conn := range c.Subscriber[response.RobotID].connections {
			msg, _ := json.Marshal(response)
			if err := conn.WriteMessage(websocket.TextMessage, msg); err != nil {
				inactiveConnections = append(inactiveConnections, id)
			}
		}
	}

	c.Mutex.Unlock()
	c.Unsubscribe(response.RobotID, inactiveConnections...)
}
