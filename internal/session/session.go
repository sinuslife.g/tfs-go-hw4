package session

import (
	"time"
)

type Session struct {
	SessionID  string
	UserID     int
	CreatedAt  time.Time
	ValidUntil time.Time
}

type Repository interface {
	Save(s Session) (Session, error)
	FindByUserID(id int) (Session, error)
}
