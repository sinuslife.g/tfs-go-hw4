package service

import (
	"fmt"
	"strconv"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/mapper"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/requests"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/responses"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/robot"
)

type RobotService struct {
	robotRepository robot.Repository
}

func NewRobotService(robotRepository robot.Repository) *RobotService {
	return &RobotService{
		robotRepository: robotRepository,
	}
}

func (s RobotService) CreateRobot(request requests.CreateRobotRequest, userID int) (responses.RobotResponse, error) {
	newRobot := robot.Robot{
		OwnerUserID: userID,
		Ticker:      request.Ticker,
		PlanStart:   &request.PlanStart.Time,
		PlanEnd:     &request.PlanEnd.Time,
		PlanYield:   request.PlanYield,
	}

	createdRobot, err := s.robotRepository.Save(newRobot)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrap(err, "cannot save robot")
	}

	return mapper.RobotEntityToResponse(createdRobot), nil
}

func (s RobotService) DeleteRobot(id string, userID string) error {
	robotEntity, err := s.robotRepository.FindByID(id)
	if err != nil {
		return errors.Wrapf(err, "[robotService.DeleteRobot] cannot find robot with id = %s", id)
	}

	if fmt.Sprint(robotEntity.OwnerUserID) != userID {
		return errors.New("[robotService.DeleteRobot] userID in request and OwnerUserID in entity do not match")
	}

	err = s.robotRepository.Delete(id)
	if err != nil {
		return errors.Wrap(err, "[robotService.DeleteRobot] error occurred while trying delete robot")
	}

	return nil
}

func (s RobotService) GetAllRobots(userID string, ticker string) ([]responses.RobotResponse, error) {
	if !isEmpty(userID) && !isEmpty(ticker) {
		robots, err := s.robotRepository.FindAllByUserIDAndTicker(userID, ticker)
		if err != nil {
			return nil, errors.Wrap(err, "[robotService.GetAllRobots] error occurred while trying to find robots by userID and ticker")
		}

		return mapper.RobotEntitiesToResponses(robots), nil
	}

	if !isEmpty(userID) {
		robots, err := s.robotRepository.FindAllByUserID(userID)
		if err != nil {
			return nil, errors.Wrap(err, "[robotService.GetAllRobots] error occurred while trying to find robots by userID")
		}

		return mapper.RobotEntitiesToResponses(robots), nil
	}

	if !isEmpty(ticker) {
		robots, err := s.robotRepository.FindAllByTicker(ticker)
		if err != nil {
			return nil, errors.Wrap(err, "[robotService.GetAllRobots] error occurred while trying to find robots by ticker")
		}

		return mapper.RobotEntitiesToResponses(robots), nil
	}

	robots, err := s.robotRepository.FindAll()
	if err != nil {
		return nil, errors.Wrap(err, "[robotService.GetAllRobots] error occurred while trying to find all robots")
	}

	return mapper.RobotEntitiesToResponses(robots), nil
}

func isEmpty(s string) bool {
	return len(s) == 0 || s == "nil" || s == "null"
}

func (s RobotService) AddFavorite(robotID string, userID string) (responses.RobotResponse, error) {
	robotEntity, err := s.robotRepository.FindByID(robotID)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrapf(err, "[robotService.AddFavorite] cannot find robot with ID = %s", robotID)
	}

	ownerUserID, err := strconv.Atoi(userID)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrapf(err, "[robotService.AddFavorite] error occurred while trying to parse userID = %s", userID)
	}

	var newRobot robot.Robot

	if robotEntity.OwnerUserID == ownerUserID {
		robotEntity.IsFavorite = true
		newRobot = robotEntity
	} else {
		newRobot = robot.Robot{
			OwnerUserID:   ownerUserID,
			ParentRobotID: &robotEntity.RobotID,
			IsFavorite:    true,
			IsActive:      false,
			Ticker:        robotEntity.Ticker,
			PlanStart:     robotEntity.PlanStart,
			PlanEnd:       robotEntity.PlanEnd,
			PlanYield:     robotEntity.PlanYield,
		}
	}

	savedRobot, err := s.robotRepository.Save(newRobot)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrap(err, "[robotService.AddFavorite] error occurred while trying save new robot entity")
	}

	return mapper.RobotEntityToResponse(savedRobot), nil
}

func (s RobotService) ActivateRobot(robotID string, userID string) (responses.RobotResponse, error) {
	robotEntity, err := s.robotRepository.FindByID(robotID)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrapf(err, "[robotService.ActivateRobot] cannot find robot by ID = %s", robotID)
	}

	if userID != fmt.Sprint(robotEntity.OwnerUserID) {
		return responses.RobotResponse{}, errors.Wrapf(err, "[robotService.ActivateRobot] userID in request and OwnerUserID in entity do not match")
	}

	now := time.Now()
	if robotEntity.IsActive || now.After(*robotEntity.PlanStart) {
		return responses.RobotResponse{}, errors.New("[robotService.ActivateRobot] business logic validation error: robot status already is active or current time not fall into plan interval")
	}

	robotEntity.IsActive = true
	robotEntity.ActivatedAt = &now

	savedRobot, err := s.robotRepository.Save(robotEntity)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrap(err, "[robotService.ActivateRobot] some error occurred while trying to save robot")
	}

	return mapper.RobotEntityToResponse(savedRobot), nil
}

func (s RobotService) DeactivateRobot(robotID string, userID string) (responses.RobotResponse, error) {
	robotEntity, err := s.robotRepository.FindByID(robotID)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrap(err, "[robotService.DeactivateRobot] some error occurred while trying to find robot")
	}

	if userID != fmt.Sprint(robotEntity.OwnerUserID) {
		return responses.RobotResponse{}, errors.Wrapf(err, "[robotService.DeactivateRobot] userID in request and OwnerUserID in entity do not match")
	}

	now := time.Now()
	if !robotEntity.IsActive || (now.After(*robotEntity.PlanStart) && now.Before(*robotEntity.PlanEnd)) {
		return responses.RobotResponse{}, errors.New("[robotService.DeactivateRobot] business logic validation error: robot already deactivated or current time not fall into plan interval")
	}

	robotEntity.IsActive = false
	robotEntity.DeactivatedAt = &now

	savedRobot, err := s.robotRepository.Save(robotEntity)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrap(err, "[robotService.DeactivateRobot] some error occurred while trying to save robot")
	}

	return mapper.RobotEntityToResponse(savedRobot), nil
}

func (s RobotService) GetRobot(robotID string, userIDFromToken string) (responses.RobotResponse, error) {
	r, err := s.robotRepository.FindByID(robotID)
	if err != nil {
		return responses.RobotResponse{}, errors.Wrap(err, "[robotService.GetRobot] some error occurred while trying to find robot")
	}

	if fmt.Sprint(r.OwnerUserID) != userIDFromToken {
		return responses.RobotResponse{}, errors.New("[robotService.GetRobot] owner user id and user id from token ddo not match")
	}

	return mapper.RobotEntityToResponse(r), nil
}
