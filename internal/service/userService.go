package service

import (
	"time"

	"github.com/pkg/errors"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/exception"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/mapper"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/requests"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/responses"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/session"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/user"
)

const ExpirationTokenMinutes = 30

type UserService struct {
	userRepository    user.Repository
	encryptor         user.PasswordEncryptor
	sessionRepository session.Repository
	tokenService      TokenService
}

func NewUserService(userRepository user.Repository, encryptor user.PasswordEncryptor, sessionRepository session.Repository, tokenService TokenService) *UserService {
	return &UserService{
		userRepository:    userRepository,
		encryptor:         encryptor,
		sessionRepository: sessionRepository,
		tokenService:      tokenService,
	}
}

func (s UserService) CreateUser(request requests.CreateUserRequest) (responses.UserResponse, error) {
	_, err := s.userRepository.GetByEmail(request.Email)
	if err == nil {
		return responses.UserResponse{}, exception.UserAlreadyExistError{Email: request.Email, Cause: err}
	}

	newUser := user.User{
		Email:     request.Email,
		Password:  s.encryptor.Encode(request.Password),
		FirstName: request.FirstName,
		LastName:  request.LastName,
		Birthday:  request.Birthday.Time,
	}

	savedUser, err := s.userRepository.Save(newUser)
	if err != nil {
		return responses.UserResponse{}, errors.Wrap(err, "cannot save user")
	}

	response := mapper.UserEntityToResponse(savedUser)

	return response, nil
}

func (s UserService) AuthorizeUser(request requests.AuthorizeRequest) (session.Session, error) {
	foundedUser, err := s.userRepository.GetByEmail(request.Email)
	if err != nil {
		return session.Session{}, exception.UserNotFoundError{Email: request.Email, Cause: err}
	}

	if !(s.encryptor.Encode(request.Password) == foundedUser.Password) {
		return session.Session{}, exception.InvalidPasswordError{}
	}

	token, _ := s.tokenService.GenerateToken(foundedUser.ID)
	userSession := session.Session{
		SessionID:  token,
		UserID:     foundedUser.ID,
		ValidUntil: time.Now().Add(time.Minute * ExpirationTokenMinutes),
	}

	savedSession, _ := s.sessionRepository.Save(userSession)

	return savedSession, nil
}

func (s UserService) UpdateUser(id int, request requests.CreateUserRequest) (responses.UserResponse, error) {
	u, _ := s.userRepository.GetByID(id)

	updatedUser := user.User{
		ID:        u.ID,
		Email:     request.Email,
		Password:  s.encryptor.Encode(request.Password),
		FirstName: request.FirstName,
		LastName:  request.LastName,
		Birthday:  request.Birthday.Time,
	}

	savedUser, err := s.userRepository.Update(updatedUser)
	if err != nil {
		return responses.UserResponse{}, errors.Wrap(err, "cannot save user")
	}

	response := mapper.UserEntityToResponse(savedUser)

	return response, nil
}

func (s UserService) GetUser(id int) (responses.UserResponse, error) {
	foundedUser, err := s.userRepository.GetByID(id)
	if err != nil {
		return responses.UserResponse{}, errors.Wrap(err, "[userService.GetUser] error occurred while trying to find user")
	}

	response := mapper.UserEntityToResponse(foundedUser)

	return response, nil
}
