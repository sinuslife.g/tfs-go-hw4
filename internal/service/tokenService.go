package service

import (
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"

	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/exception"
	"gitlab.com/sinuslife.g/tfs-go-hw4/internal/session"
)

const (
	BearerStringNumberTokens = 2
)

type TokenService struct {
	secret            string
	sessionRepository session.Repository
}

type Token struct {
	UserID int
	Now    time.Time
	jwt.StandardClaims
}

func NewTokenService(secret string, repository session.Repository) *TokenService {
	return &TokenService{
		secret:            secret,
		sessionRepository: repository,
	}
}

func (s TokenService) GenerateToken(id int) (string, error) {
	t := Token{
		UserID: id,
		Now:    time.Now(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, t)

	return token.SignedString([]byte(s.secret))
}

func (s TokenService) ValidateToken(token string) (int, error) {
	splittedToken := strings.Split(token, " ")
	if len(splittedToken) != BearerStringNumberTokens {
		return 0, exception.InvalidAuthorizationHeaderError{}
	}

	tokenString := splittedToken[1]

	t := &Token{}
	jwtToken, err := jwt.ParseWithClaims(tokenString, t, func(token *jwt.Token) (interface{}, error) {
		return []byte(s.secret), nil
	})

	if err != nil {
		return 0, errors.Wrap(err, "[tokenService.ValidateToken] some error occurred while trying to parse token")
	}

	if !jwtToken.Valid {
		return 0, errors.New("[tokenService.ValidateToken] token is not valid")
	}

	foundedSession, err := s.sessionRepository.FindByUserID(t.UserID)
	if err != nil {
		return 0, errors.Wrapf(err, "[tokenService.ValidateToken] cannot find session by id = %d", t.UserID)
	}

	if foundedSession.SessionID != tokenString {
		return 0, errors.New("[tokenService.ValidateToken] token and sessionID in founded session is not match")
	}

	if time.Now().After(foundedSession.ValidUntil) {
		return 0, errors.New("[tokenService.ValidateToken] token is expired")
	}

	return foundedSession.UserID, nil
}
