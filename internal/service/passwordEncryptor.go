package service

import (
	"crypto/sha256"
	"fmt"
)

type SHA256PassEncryptor struct {
}

func (SHA256PassEncryptor) Encode(password string) string {
	encryptPass := sha256.Sum256([]byte(password))
	return fmt.Sprintf("%x", encryptPass)
}
