# Tinkoff fintech 2020. Golang
## Проект торговая платформа роботов
**Общая идея**   
Пользователи регистрируются в сервисе. 
Они могут создавать торговых роботов, добавлять их в избранное или добавлять в избранное роботов других пользователей.
При создании робота указывается цена покупки, цена продажи, планируемая доходность, а также временное окно, в котором будет торговать робот.
В процессе работы, доходность робота будет изменяться. По окончанию работы, роботу присваивается фактическая доходность.
На странице с роботом открывается WebSocket-соединение, которое показывает в режиме онлайн текущую доходность робота и кол-во активированных роботов в избранном всех пользователей.
Робот подключается к торговой системе по средствам gRPC

[Требования к проекту](https://gitlab.com/slutoshin/tfs20s/-/blob/master/project-requirements.md)  
[Техническое задание к проекту "ярмарка торговых роботов"](https://gitlab.com/slutoshin/tfs20s/-/blob/master/project.md)  
[Swagger](https://app.swaggerhub.com/apis/viktorkomarov/tfs20/1.0.0#/)

## Языки, технологии, фреймворки

Сервис написан на языке Golang с использованием библиотек zap, chi, gorm. 
Данные пользователю отдаются по rest и websocket. 
Пользовательская информация хранится в PostgreSQL. 
Подключение к внешнему сервисы, для получения информации о цене тикера, происходит по gRPC.

## Процесс работы программы
**Функционал**
1. Регистрация пользователя
1. Авторизация и аутентификация пользователя
1. Обновление профиля пользователя
1. Создание робота
1. Удаление робота
1. Получение списка созданных пользователем роботов
1. Получение каталога роботов
1. Реализация фильтров в каталоге роботов (по тикеру, по пользователю)
1. Получение деталей робота по идентификатору
1. Добавление робота в избранное пользователем
1. Активация робота, добавленного в избранное
1. Деактивация робота, добавленного в избранное
1. Реализация процесса, управляющего жизненным циклом робота

Основной функционал добавления, удаления, редактирования сущностей:
1. Данные приходят от пользователя в формате json
1. Происходит валидация токена пользователя, если неверный выкидывается ошибка
1. В сервисе бизнес логики формируется и отправляется запрос в базу
1. Валидируется ответ от БД
1. Формируетя ответ пользователю
1. Ответ отправляется

Получение обновления данных по Websocket:
1. Пользователь отправляет ws запрос на специальную ручку **/ws
1. Сервис получает текущие данные из БД и отправляет пользователю
1. Происходит добавления ws-коннекта пользователя как подписчика в сервис подписок
1. При получении нового сообщения сервис подписок отправляет всем подписчикам новые данные

Работа сервиса роботов:
1. Сервис опрашивает БД для получения активных роботов
1. Робот запускается в отдельно горутине
1. Робот запрашивает по gRPC данные о цене, совершает сделки

## Запуск

Для запуска необходим Docker и утилита Make 

Запуск сервисов:
```
make start
```
Остановка сервисов:
```
make stop
```

