package log

import "go.uber.org/zap"

type Logger interface {
	Debug(message string)
	Debugf(format string, args ...interface{})

	Warn(message string)
	Warnf(format string, args ...interface{})

	Error(message string)
	Errorf(format string, args ...interface{})

	Fatal(message string)
	Fatalf(format string, args ...interface{})
}

func New() Logger {
	logger, _ := zap.NewDevelopment()
	return &ZapLoggerWrapper{zapLogger: *logger}
}

type ZapLoggerWrapper struct {
	zapLogger zap.Logger
}

func (l *ZapLoggerWrapper) Debug(message string) {
	l.zapLogger.Debug(message)
}

func (l *ZapLoggerWrapper) Debugf(format string, args ...interface{}) {
	l.zapLogger.Sugar().Debugf(format, args...)
}

func (l *ZapLoggerWrapper) Warn(message string) {
	l.zapLogger.Warn(message)
}

func (l *ZapLoggerWrapper) Warnf(format string, args ...interface{}) {
	l.zapLogger.Sugar().Warnf(format, args...)
}

func (l *ZapLoggerWrapper) Error(message string) {
	l.zapLogger.Error(message)
}

func (l *ZapLoggerWrapper) Errorf(format string, args ...interface{}) {
	l.zapLogger.Sugar().Errorf(format, args...)
}

func (l *ZapLoggerWrapper) Fatal(message string) {
	l.zapLogger.Fatal(message)
}

func (l ZapLoggerWrapper) Fatalf(format string, args ...interface{}) {
	l.zapLogger.Sugar().Fatalf(format, args...)
}
